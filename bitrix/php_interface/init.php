<?

use  Bitrix\Sale;
function f12($arr){
	echo "<script>console.log(".json_encode($arr).")</script>";
}



Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'onSaleDeliveryServiceCalculate', 'yourHandler');

function yourHandler(\Bitrix\Main\Event $event)
{
	$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
    // $order = $event->getParameter('BASKET');
    $calcResult = $event->getParameter('RESULT');
    $shipment = $event->getParameter('SHIPMENT');
    if($shipment != false && $calcResult != false){
    $deliv_id = $shipment->getDeliveryId();
    $sum = $basket->getprice();

    $price_cust = false;
    switch ($deliv_id) {
       // ------------------------- Доставка в пределах МКАД
      case 16:
        if($sum > 5000) {
          $price_cust = '0';
        }else{
          $price_cust = '400';
        }
        break;
       // ------------------------- Доставка за пределы МКАД
      case 17:
        if($sum > 5000) {
          $price_cust = '0';
        }else{
          $price_cust = '400';
        }
        break;
       // ------------------------- Доставка Люберцы, Жулебино, Котельники, Дзержинский
      case 18:
        if($sum > 3000) {
          $price_cust = '0';
        }else{
          $price_cust = '400';
        }
        break;
     
      default:
        # code...
        break;
    }

    if($price_cust != false)
      $calcResult->setDeliveryPrice($price_cust);

  }

    return new \Bitrix\Main\EventResult(
        \Bitrix\Main\EventResult::SUCCESS,
        array(
            "RESULT" => $calcResult,
        )
    );
}


// генерация символьных кодов

// AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "AddElementOrSectionCode"); 
// AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "AddElementOrSectionCode"); 
AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", "AddElementOrSectionCode"); 
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", "AddElementOrSectionCode"); 

function AddElementOrSectionCode(&$arFields) { 
   $params = array(
      "max_len" => "100", 
      "change_case" => "L", 
      "replace_space" => "_", 
      "replace_other" => "_", 
      "delete_repeat_replace" => "true", 
      "use_google" => "false", 
   );
   
   if (strlen($arFields["NAME"])>0 && strlen($arFields["CODE"])<=0 ) {
      $arFields['CODE'] = CUtil::translit($arFields["NAME"], "ru", $params);    
   }
}
