<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	{
		die();
	}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CJSCore::Init(array("jquery"));
$this->addExternalCss("//astatic.nodacdn.net/css/goods.info.css");
$this->addExternalCss("//astatic.nodacdn.net/css/paginator.css");
$this->addExternalCss("//astatic.nodacdn.net/css/search.by.number.brand.results.css");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/common.css");
$this->addExternalCss("//astatic.nodacdn.net/css/goods.catalog.css");
$this->addExternalCss("//astatic.nodacdn.net/css/cssFramework.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/tecdoc.js");
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].$templateFolder."/template.php");
?>
<div class="tecdocMain catalogTecdoc">
	<div class="tecdocTitle">
		<br>
		<?=GetMessage("NTD_AVTO_DETAIL")?>  <b><?= $arResult["MODIFICATION"]["name"] ?></b>
	</div>


	<? if ($arResult["ERROR"] == ""): ?>
		<script>
			/*������������� ������ ���� ��� ���������� ������. ������ ��� ���������� ������*/
			var catalogHead = $('.wGoodsGroupTile').find('div.goodsGroupHead');
			var catalogBody = $('.wGoodsGroupTile').find('div.goodsBody');

			function calculateWidth() {
				var catalogTileWidth = $('.wGoodsGroup').find('li.item').width() + 12;
				var goodsGroupWidth = $('.wGoodsGroup').width();
				$('.wGoodsGroupTile').find('div.goodsGroupHead').css({'width': Math.floor((goodsGroupWidth - 20) / catalogTileWidth) * catalogTileWidth + 20 + "px"});
				$('.wGoodsGroupTile').find('div.goodsBody').css({'width': Math.floor((goodsGroupWidth - 20) / catalogTileWidth) * catalogTileWidth + 20 + 'px'});
			}

			$(function () {
				calculateWidth();
			});

			$(window).resize(function () {
				catalogHead.css('width', '0');
				catalogBody.css('width', '0');
				calculateWidth();
			});
		</script>


		<div class="wGoodsGroup wGoodsGroupTile">
			<div class="goodsGroupHead" style="width: 1070px;">
				<noindex>
					<div class="rightControls">
						<div class="showingTypes">
							<a class="showing-tile <? if ($arResult["viewMode"] != "list")
								echo 'showing-act' ?>" title="<?=GetMessage("NTD_CHOW_TYPE_1")?>"
							   href="<?= $APPLICATION->GetCurPageParam("", array("viewMode")) ?>" rel="nofollow"><span
									class="fr-icon-layout"></span></a>
							<a class="showing-list <? if ($arResult["viewMode"] == "list")
								echo 'showing-act' ?>" title="<?=GetMessage("NTD_CHOW_TYPE_2")?>"
							   href="<?= $APPLICATION->GetCurPageParam("viewMode=list", array("viewMode")) ?>" rel="nofollow"><span
									class="fr-icon-list"></span></a>
						</div>
						<div class="showOnPage">
							<?=GetMessage("NTD_COUNT_PAGE")?><br>
							<? foreach($arResult["PAGES"] as $page): ?>
								<? if($page==$arResult["PAGE"]): ?>
									<b><?=$page?></b>
								<? else: ?>
									<a href="<?=$APPLICATION->GetCurPageParam("limit=".$page,array("limit"))?>"
									   title="<?=GetMessage("NTD_COUNT_PAGE")?> <?=$page?>" rel="nofollow"><?=$page?></a>
								<? endif ?>
							<? endforeach ?>

						</div>
					</div>
				</noindex>
			</div>
			<div class="goodsBody tileModeBody" style="width: 1070px;">
				<? if (!empty($arResult["ITEMS"])): ?>
					<? if ($arResult["viewMode"] != "list"): ?>
						<ul class="item_ul">

							<? foreach ($arResult["ITEMS"] as $arItem): ?>

								<li class="item">
									<div class="topBlock">
										<div class="articlePic fr-flex fr-flex-center fr-flex-middle">
											<div class="articleImages">
												<div class="article-image">
													<? if (!empty($arItem["SRC"])): ?>
														<img src="<?= $arItem["SRC"] ?>"/>
													<? else: ?>
														<img src="//astatic.nodacdn.net/common.images/noImage.png"/>
													<? endif ?>
												</div>
											</div>
										</div>
										<div class="articleDesc">
											<h3><?= $arItem["BRAND"] ?></h3>
											<a target="_blank" href="<?= $arItem["LINK"] ?>">
												<?= $arItem["ART"] ?>
												<br>
												<?= $arItem["NAME"] ?>
											</a>
										</div>
									</div>
									<div class="order">
										<div class="priceButton">
											<a target="_blank" class="fr-btn fr-btn-primary"
											   href="<?= $arItem["LINK"] ?>"><?=GetMessage("NTD_SHOW_PRICE")?></a>
										</div>
									</div>
								</li>
							<? endforeach ?>
						</ul>

					<? else: ?>
						<div class="fr-table-responsive">
							<table class="fr-table fr-table-bordered fr-table-condensed fr-table-hover catalogListModeTable">
								<thead>
								<tr>
									<th></th>
									<th><?=GetMessage("NTD_FIRM_DETAIL")?></th>
									<th><?=GetMessage("NTD_CODE_DETAIL")?></th>
									<th><?=GetMessage("NTD_MODEL_DETAIL")?></th>
									<th></th>

								</tr>
								</thead>
								<tbody>
								<? foreach ($arResult["ITEMS"] as $arItem): ?>

									<tr class="white item">
										<td>
											<div class="articlePicList">
												<div class="articleImages">
													<div class="article-image">
														<a href="#" data-fancybox-group="group56c5ad0ecfc2e" rel="nofollow">
															<? if (!empty($arItem["SRC"])): ?>
																<img src="<?= $arItem["SRC"] ?>"/>
															<? else: ?>
																<img src="//astatic.nodacdn.net/common.images/noImage.png"/>
															<? endif ?>
														</a>
													</div>


												</div>
											</div>
										</td>
										<!--�����-->
										<td>
											<?= $arItem["BRAND"] ?>
										</td>
										<!--��� ������-->
										<td>
											<?= $arItem["ART"] ?>
										</td>
										<!--������-->
										<td class="description">
											<a href="<?= $arItem["LINK"] ?>" target="_blank">
												<?= $arItem["NAME"] ?>
											</a>
										</td>
										<!--����-->
										<td class="order orderW">
											<a target="_blank" class="fr-btn fr-btn-primary"
											   href="<?= $arItem["LINK"] ?>"><?=GetMessage("NTD_SHOW_PRICE")?></a>
										</td>
									</tr>
								<? endforeach ?>

								</tbody>
							</table>
						</div>
					<? endif ?>
					<? if($arResult["COUNT"]>$arResult["PAGE"]) echo $arResult["NAV_STRING"];?>
				<? endif ?>

			</div>
		</div>
		<div id="dialogConfirm"></div>
	<? else: ?>
		<p style="text-align: center"><?=GetMessage("NTD_SELECT_PROP")?></p>
	<? endif ?>
</div>