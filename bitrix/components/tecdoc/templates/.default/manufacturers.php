<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	{
		die();
	}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs("https://yastatic.net/jquery/1.7.2/jquery.min.js");
$this->addExternalJs("https://yastatic.net/jquery-ui/1.8.16/jquery-ui.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/ui/custom-theme/jquery-ui-1.9.2.custom.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/ui/jquery-ui-i18n.min.js");
$this->addExternalJs("https://yastatic.net/jquery/fancybox/2.1.4/jquery.fancybox.min.js");
$this->addExternalCss("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.fancybox.css");
$this->addExternalJs("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.mousewheel-3.0.2.pack.js");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl_minimized.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.easing.1.3.js");

$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/tecdoc.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/common.css");
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].$templateFolder."/template.php");
?>
<div class="tecdocMain catalogTecdoc">

	<div class="tecdocTop">
		<script>

			$(document).ready(function () {
				$.data(document.body, 'searchTecdocMode', 'models');
			});
		</script>
		<input placeholder="<?=GetMessage("NTD_SERCH_PLACEHOLDER")?>" value="" id="searchTecdoc" name="searchTecdoc"></div>

	<div class="clearfix catalogTabs">
		<ul class="tabs_table clearfix">
			<li class="<? if(!intval($_REQUEST["year_filter"])) echo "active" ?>"><a href="<?= $arResult["URL"] ?>"><?=GetMessage("NTD_ALL")?></a></li>
			<? foreach ($arResult["TABS"] as $param => $name): ?>
				<li class="<? if($param==intval($_REQUEST["year_filter"])) echo "active" ?>">
					<a href="<?= $APPLICATION->GetCurPageParam("year_filter=".$param, array("year_filter")) ?>">
						<?= $name ?>
					</a>
				</li>
			<? endforeach ?>
		</ul>
	</div>

	<div class="tecdocTable">
		<div class="tecdocTitleBlock">
			<ul class="tecdocModelTitle">
				<li class="couple">
					<div class="tecdocModel"><?=GetMessage("NTD_MODEL")?></div>
					<div class="tecdocYear"><?=GetMessage("NTD_YEAR_CREATE")?></div>
				</li>
				<li class="couple">
					<div class="tecdocModel"><?=GetMessage("NTD_MODEL")?></div>
					<div class="tecdocYear"><?=GetMessage("NTD_YEAR_CREATE")?></div>
				</li>
				<li class="couple">
					<div class="tecdocModel"><?=GetMessage("NTD_MODEL")?></div>
					<div class="tecdocYear"><?=GetMessage("NTD_YEAR_CREATE")?></div>
				</li>
			</ul>
		</div>
		<div class="tecdocTableBlock">
			<ul class="ulTecdocModels">
				<?
				$arYears=array(
					0=>array(1000,3000),
					1980=>array(1000,1990),
					1990=>array(1990,2000),
					2000=>array(2000,2010),
					2010=>array(2010,3010),
				);
				$year=intval($_REQUEST["year_filter"]);
				?>
				<? foreach ($arResult["MODELS"] as $model): ?>
					<? if($arYears[$year][0]<=$model["year"] && $model["year"]<=$arYears[$year][1]): ?>
						<li class="couple trSearch shown" style="display: inline-block;">
							<div class="forSearch tecdocModel">
								<a href="<?= $APPLICATION->GetCurPageParam("model=" . $model["id"], array("year_filter")) ?>">
									<?= $model["name"] ?>
								</a>
							</div>
							<div class="forSearch tecdocYear">
								<?= $model["yearFrom"] ?> - <?= $model["yearTo"] ?>
							</div>
						</li>
					<? endif ?>
				<? endforeach ?>

			</ul>
		</div>
	</div>
</div>