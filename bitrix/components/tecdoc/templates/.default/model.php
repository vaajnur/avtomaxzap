<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$this->addExternalJs("https://yastatic.net/jquery/1.7.2/jquery.min.js");
$this->addExternalJs("https://yastatic.net/jquery-ui/1.8.16/jquery-ui.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/ui/custom-theme/jquery-ui-1.9.2.custom.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/ui/jquery-ui-i18n.min.js");
$this->addExternalJs("https://yastatic.net/jquery/fancybox/2.1.4/jquery.fancybox.min.js");
$this->addExternalCss("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.fancybox.css");
$this->addExternalJs("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.mousewheel-3.0.2.pack.js");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl_minimized.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.easing.1.3.js");

$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/tecdoc.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/common.css");
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].$templateFolder."/template.php");

?>

<div class="tecdocMain catalogTecdoc">

	<div class="tecdocTop">
		<script>

			$(document).ready(function () {
				$.data(document.body, 'searchTecdocMode', '');
			});
		</script>
		<input placeholder="<?=GetMessage("NTD_SERCH_PLACEHOLDER")?>" value="" id="searchTecdoc" name="searchTecdoc">
	</div>
	<div class="fr-table-responsive">
		<table class="tecdocTable">
			<tbody><tr class="tecdocThead">
				<th><?=GetMessage("NTD_MODIFICATION")?></th>
				<th><?=GetMessage("NTD_YEAR_CREATE")?></th>
				<th><?=GetMessage("NTD_CODE_DVIG")?></th>
				<th><?=GetMessage("NTD_CUZOV")?></th>
				<th><?=GetMessage("NTD_DVIG")?></th>
				<th><?=GetMessage("NTD_POWER")?></th>
				<th><?=GetMessage("NTD_OIL")?></th>
			</tr>
			<?foreach($arResult["MODELS"] as $model):?>
				<?
				$model["yearFrom"]=substr($model["yearFrom"],4,6)."/".substr($model["yearFrom"],0,4);
				$model["yearTo"]=substr($model["yearTo"],4,6)."/".substr($model["yearTo"],0,4);
				?>
				<tr class="trSearch">
					<td class="forSearch">
						<a href="<?= $APPLICATION->GetCurPageParam("modelVariant=" . $model["id"], array("year_filter","modelVariant")) ?>">
							<?=$model["name"]?>
						</a>
					</td>
					<td class="forSearch"><?= $model["yearFrom"] ?> - <?= $model["yearTo"] ?></td>
					<td class="forSearch"><?=$model["motorCodes"]?></td>
					<td class="forSearch"><?=$model["constructionType"]?></td>
					<td class="forSearch"><?=number_format($model["cylinderCapacityLiter"]/100,1)?><?=GetMessage("NTD_LITR")?></td>
					<td class="forSearch"><?=$model["powerHP"]?><?=GetMessage("NTD_POWER_CO")?></td>
					<td class="forSearch"><?=$model["fuelType"]?></td>
				</tr>
			<? endforeach ?>

			</tbody></table>
	</div>
</div>