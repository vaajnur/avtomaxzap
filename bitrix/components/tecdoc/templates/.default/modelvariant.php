<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalJs("https://yastatic.net/jquery/1.7.2/jquery.min.js");
$this->addExternalJs("https://yastatic.net/jquery-ui/1.8.16/jquery-ui.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/ui/custom-theme/jquery-ui-1.9.2.custom.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/ui/jquery-ui-i18n.min.js");
$this->addExternalJs("https://yastatic.net/jquery/fancybox/2.1.4/jquery.fancybox.min.js");
$this->addExternalCss("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.fancybox.css");
$this->addExternalJs("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.mousewheel-3.0.2.pack.js");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl_minimized.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.easing.1.3.js");

$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/tecdoc.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/common.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/dtree.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/dtree.css");
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].$templateFolder."/template.php");

?>
<div class="tecdocMain catalogTecdoc">
	<div class="tecdocTop">
		<script>

			$(document).ready(function () {
				$.data(document.body, 'searchTecdocMode', 'tree');
			});
		</script>
		<input placeholder="<?=GetMessage("NTD_SERCH_PLACEHOLDER")?>" value="" id="searchTecdoc" name="searchTecdoc"></div>
	<div class="tecdocTitle">
		<?=GetMessage("NTD_AVTO_DETAIL")?> <b><?=$arResult["MODIFICATION"]["name"]?></b>
	</div>
	<div class="tecdocTreeCols">
		<div class="tecdocCol9">

			<div style="width: 100%;" class="dtree">
				<p><a href="javascript: tree.openAll();"><?=GetMessage("NTD_OPEN_ALL")?></a> | <a href="javascript: tree.closeAll();"><?=GetMessage("NTD_CLOSE_ALL")?></a></p>
				<script>
					tree = new dTree('tree', '//astatic.nodacdn.net/common.images/dtree//');
					tree.icon.root = '//astatic.nodacdn.net/common.images/dtree//car.png';
					tree.add(1, -1, '<?=GetMessage("NTD_GROUP_DETAIL")?>', '', false, false, false, false, false, false, false, true);
					<? foreach(	$arResult["TREE"] as $tree): ?>
					tree.add(<?=$tree["id"]?>, <?if(is_null($tree["parentId"])):?>1<?else:?><?=$tree['parentId']?><? endif ?>, '<?=$tree["name"]?>', '<?if (!$tree["hasChilds"]):?><?=$APPLICATION->GetCurPageParam("group=".$tree["id"],array());?><? endif ?>', false, false, false, false, false, false, false, <?if ($tree["hasChilds"]):?>true<?else:?>false<? endif?>);
					<? endforeach ?>

					document.write(tree);
				</script>
			</div>

		</div>
		<div class="tecdocCol3">
			<div class="tecdocCt">
				<span><?=GetMessage("NTD_POPULAR_CAT")?></span>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100597",array());?>"><?=GetMessage("NTD_CROUP_1")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100121",array());?>"><?=GetMessage("NTD_CROUP_2")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100042",array());?>"><?=GetMessage("NTD_CROUP_3")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100337",array());?>"><?=GetMessage("NTD_CROUP_4")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100088",array());?>"><?=GetMessage("NTD_CROUP_5")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100103",array());?>"><?=GetMessage("NTD_CROUP_6")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100260",array());?>"><?=GetMessage("NTD_CROUP_7")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100350",array());?>"><?=GetMessage("NTD_CROUP_8")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100743",array());?>"><?=GetMessage("NTD_CROUP_81")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100566",array());?>"><?=GetMessage("NTD_CROUP_9")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100356",array());?>"><?=GetMessage("NTD_CROUP_10")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=101994",array());?>"><?=GetMessage("NTD_CROUP_11")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100150",array());?>"><?=GetMessage("NTD_CROUP_12")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100048",array());?>"><?=GetMessage("NTD_CROUP_13")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100470",array());?>"><?=GetMessage("NTD_CROUP_14")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100431",array());?>"><?=GetMessage("NTD_CROUP_15")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100104",array());?>"><?=GetMessage("NTD_CROUP_16")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100601",array());?>"><?=GetMessage("NTD_CROUP_17")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100151",array());?>"><?=GetMessage("NTD_CROUP_18")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100359",array());?>"><?=GetMessage("NTD_CROUP_19")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100206",array());?>"><?=GetMessage("NTD_CROUP_20")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100095",array());?>"><?=GetMessage("NTD_CROUP_21")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100717",array());?>"><?=GetMessage("NTD_CROUP_22")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100261",array());?>"><?=GetMessage("NTD_CROUP_23")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=102208",array());?>"><?=GetMessage("NTD_CROUP_24")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100032",array());?>"><?=GetMessage("NTD_CROUP_25")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100030",array());?>"><?=GetMessage("NTD_CROUP_26")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100035",array());?>"><?=GetMessage("NTD_CROUP_27")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100263",array());?>"><?=GetMessage("NTD_CROUP_28")?></a><br>
				<a href="<?=$APPLICATION->GetCurPageParam("group=100754",array());?>"><?=GetMessage("NTD_CROUP_29")?></a><br>
			</div>
		</div>
	</div>
</div>