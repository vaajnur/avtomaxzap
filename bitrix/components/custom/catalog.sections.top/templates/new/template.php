<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="catalog-sections-top">
<?

// print_r($arResult["SECTIONS"]);
foreach($arResult["SECTIONS"] as $arSection):
    // echo mydump($arSection['PICTURE']);
    // echo mydump($arSection['PREVIEW_PICTURE']);
    ?>
		<div id="bx_1847241719_27891" class="section-popular-item col-sm-4 col-xs-6">
    <div class="gr-section-item">
        <div class="gr-section-img">
            <a href="<?=$arSection['SECTION_PAGE_URL'];?>">
                <img class="img-responsive" alt="<?=$arSection['NAME'];?>" title="<?=$arSection['NAME'];?>" src="<?=$arSection['PICTURE_SRC'];?>">
            </a>
        </div>
        <div class="gr-section-bottom">
            <ul class="gr-section-name">
                <li class="section-title"><a href="<?=$arSection['SECTION_PAGE_URL'];?>" title="<?=$arSection['NAME'];?>"><?=$arSection['NAME'];?></a></li>
            	<?foreach($arSection["SUB_SECTIONS"] as $k => $subSection):?>
                    <?//=$k;?>
	            	<li class="<?=$k>4?' hide_li ':'';?>"><a href="<?=$subSection['SECTION_PAGE_URL'];?>" title="<?=$subSection['NAME'];?>"><?=$subSection['NAME'];?></a></li>
                <?endforeach?>
            </ul>
        </div>
    </div>
</div>
<?endforeach?>
</div>
