<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="abcd-filter">
<?


    function requestUriAddGetParams(array $params)
    {
        $parseRes=parse_url($_REQUEST['REQUEST_URI']);
        $params=array_merge($_GET, $params);
        return $parseRes['path'].'?'.http_build_query($params);
    }

// print_r($arResult["NAMES"]);
foreach($arResult["NAMES"] as $letter => $names):?>
        <li class="abcd-letter"><?=$letter;?> <ul class="abcd-names">
            <? foreach($names as $name):?>
                <li class="abcd-name"><a href="<?=requestUriAddGetParams(['tag'=>$name]);?>"><?=$name;?></a></li>
            <?endforeach?>

        </ul>
    </li>
<?endforeach?>
</ul>