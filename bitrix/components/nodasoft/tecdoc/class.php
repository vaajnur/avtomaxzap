<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

class TecDoc extends CBitrixComponent
	{
    public $type;
    public $title;
	/**
	 * @var CTecdocCall
	 */
    public $Tecdoc;


    public function executeBreadCrumbs()
	    {
		    global $APPLICATION;

		    if ($this->type == "manufacturers" || $this->type == "model")
			    {
				    $params = array();
				    if ($this->arResult["CAR_TYPE"] > 0)
					    {
						    $params["carType"] = $this->arResult["CAR_TYPE"];
					    }

				    $manufacturers = $this->Tecdoc->getManufacturers($params);

				    foreach ($manufacturers as $manufactura)
					    {
						    if ($manufactura["id"] == intval($_REQUEST["man"]))
							    {
								    $this->arResult["BREDCRUMBS"][] = array(
									    "URL" => $APPLICATION->GetCurPageParam("", array(
										    "model",
										    "modelVariant",
										    "group",
										    "viewMode",
										    "year_filter",
										    "page"
									    )),
									    "NAME" => $manufactura["name"],
								    );
								    $this->title = $manufactura["name"];
							    }
					    }
			    }

		    if ($this->type == "model")
			    {
				    $models = $this->Tecdoc->getModels(intval($_REQUEST["man"]));
				    foreach ($models as $model)
					    {
						    if ($model["id"] == intval($_REQUEST["model"]))
							    {
								    $this->arResult["BREDCRUMBS"][] = array(
									    "URL" => $APPLICATION->GetCurPageParam("", array(
										    "modelVariant",
										    "group",
										    "viewMode",
										    "year_filter",
										    "page"
									    )),
									    "NAME" => $model["name"],
								    );
								    $this->title = $model["name"];
							    }
					    }
			    }
		    if ($this->type == "modelvariant" || $this->type == "group")
			    {
				    $modification = $this->Tecdoc->getModification(intval($_REQUEST["modelVariant"]));
				    $this->title = $modification[0]["name"];
				    $this->arResult["BREDCRUMBS"][] = array(
					    "URL" => $APPLICATION->GetCurPageParam("", array(
						    "model",
						    "modelVariant",
						    "group",
						    "viewMode",
						    "year_filter",
						    "page"
					    )),
					    "NAME" => $modification[0]["manufacturerName"],
				    );
				    $this->arResult["BREDCRUMBS"][] = array(
					    "URL" => $APPLICATION->GetCurPageParam("", array(
						    "modelVariant",
						    "group",
						    "viewMode",
						    "year_filter",
						    "page"
					    )),
					    "NAME" => $modification[0]["modelName"],
				    );
				    $this->arResult["BREDCRUMBS"][] = array(
					    "URL" => $APPLICATION->GetCurPageParam("", array(
						    "group",
						    "viewMode",
						    "year_filter",
						    "page"
					    )),
					    "NAME" => $modification[0]["name"],
				    );
				    $this->arResult["MODIFICATION"] = $modification[0];
			    }
		    if ($this->type == "group")
			    {
				    $tree = $this->Tecdoc->getTree(intval($_REQUEST["modelVariant"]));
				    foreach ($tree as $item)
					    {
						    if (intval($_REQUEST["group"]) == $item["id"])
							    {
								    $this->arResult["BREDCRUMBS"][] = array(
									    "URL" => $APPLICATION->GetCurPageParam("", array(
										    "viewMode",
										    "year_filter",
										    "page"
									    )),
									    "NAME" => $item["name"],
								    );
							    }
					    }
			    }
		    $APPLICATION->SetTitle($this->title);
		    foreach ($this->arResult["BREDCRUMBS"] as $bread)
			    $APPLICATION->AddChainItem($bread["NAME"], $bread["URL"]);
	    }

    public function executeType()
	    {
		    if ($this->type == "defaul")
			    {
				    $this->getDefault();
			    }
		    if ($this->type == "manufacturers")
			    {
				    $this->getManufacturers();
			    }
		    if ($this->type == "model")
			    {
				    $this->getModel();
			    }
		    if ($this->type == "modelvariant")
			    {
				    $this->getModelvariant();
			    }
		    if ($this->type == "group")
			    {
				    $this->getGroup();
			    }
	    }

    public function executeComponent()
	    {
		    global $APPLICATION;
			CModule::IncludeModule("iblock");
			CModule::IncludeModule("nodasoft.tecdoc");

			$this->Tecdoc = new CTecdocCall();


		    $this->arResult["URL"] = $APPLICATION->GetCurPage(false);
		    $this->arResult["CAR_TYPE_1"] = $APPLICATION->GetCurPageParam("carType=1", array("carType"));
		    $this->arResult["CAR_TYPE_2"] = $APPLICATION->GetCurPageParam("carType=2", array("carType"));

		    $this->arResult["CAR_TYPE"] = intval($_REQUEST["carType"]);
		    $this->arResult["LITERA"] = htmlspecialchars($_REQUEST["litera"]);

		    $this->type = "defaul";
		    $this->title = GetMessage("NTD_CATALOG");

		    $this->arResult["BREDCRUMBS"] = array(
			    array(
				    "NAME" => GetMessage("NTD_CATALOG"),
				    "URL" => $this->arResult["URL"],
			    ),
		    );

		    if (intval($_REQUEST["man"]) > 0)
			    {
				    $this->type = "manufacturers";
			    }

		    if (intval($_REQUEST["man"]) > 0 && intval($_REQUEST["model"]) > 0)
			    {
				    $this->type = "model";
			    }

		    if (isset($_REQUEST['model']) && isset($_REQUEST['man']) && isset($_REQUEST['modelVariant']))
			    {
				    $this->type = "modelvariant";
			    }

		    if (isset($_REQUEST['group']) && isset($_REQUEST['modelVariant']))
			    {
				    $this->type = "group";
			    }
		    $this->executeBreadCrumbs();
		    $this->executeType();
	    }

    public function getDefault()
	    {
		    global $APPLICATION;

		    $params = array();
		    if ($this->arResult["CAR_TYPE"] > 0)
			    {
				    $params["carType"] = $this->arResult["CAR_TYPE"];
			    }
		    $manufacturers = $this->Tecdoc->getManufacturers($params);

		    if ($manufacturers != "")
			    {

				    foreach ($manufacturers as $manufactura)
					    {
						    $liter = (string)$manufactura["name"][0];
						    if ($this->arResult["LITERA"] != "" && $liter != $this->arResult["LITERA"])
							    {
								    continue;
							    }
						    $manufactura["url"] = $APPLICATION->GetCurPageParam("man=" . $manufactura["id"], array(
							    "carType",
							    "litera"
						    ));
						    $this->arResult["MANUFACTURES"][$liter][] = $manufactura;
					    }
				    $this->IncludeComponentTemplate("template");
			    }
		    else
			    {
				    $this->IncludeComponentTemplate("404");
			    }
	    }

    public function getManufacturers()
	    {
		    global $APPLICATION;
		    $models = $this->Tecdoc->getModels(intval($_REQUEST["man"]));
		    $arTabs = array(
			    "1980" => "-1990",
			    "1990" => "1990-2000",
			    "2000" => "2000-2010",
			    "2010" => "2010-",
		    );

		    $this->arResult["TABS"] = $arTabs;
		    $this->arResult["URL"] = $APPLICATION->GetCurPageParam("", array("year_filter"));

		    if ($models != "")
			    {
				    foreach ($models as $model)
					    {
						    $model["year"] = substr($model['yearFrom'], 0, 4);
						    if ($model["yearFrom"] != "")
							    {
								    $model["yearFrom"] = substr($model["yearFrom"], 4, 6) . "/" . substr($model["yearFrom"], 0, 4);
							    }
						    if ($model["yearTo"] != "")
							    {
								    $model["yearTo"] = substr($model["yearTo"], 4, 6) . "/" . substr($model["yearTo"], 0, 4);
							    }
						    $this->arResult["MODELS"][] = $model;

					    }

				    $this->IncludeComponentTemplate("manufacturers");
			    }
		    else
			    {
				    $this->IncludeComponentTemplate("404");
			    }
	    }

    public function getModel()
	    {
		    $modifications = $this->Tecdoc->getModifications(intval($_REQUEST["model"]), intval($_REQUEST["man"]));
		    if ($modifications != "")
			    {
				    $this->arResult["MODELS"] = $modifications;
				    $this->IncludeComponentTemplate("model");
			    }
		    else
			    {
				    $this->IncludeComponentTemplate("404");
			    }
	    }

    public function getModelvariant()
	    {
		    $tree = $this->Tecdoc->getTree(intval($_REQUEST["modelVariant"]));
		    if ($tree != "")
			    {
				    $this->arResult["TREE"] = $tree;
				    $this->IncludeComponentTemplate("modelvariant");
			    }
		    else
			    {
				    $this->IncludeComponentTemplate("404");
			    }
	    }

    public function getGroup()
	    {
		    $this->arResult["ERROR"] = "";
		    $module_id = "nodasoft.tecdoc";

		    $IBLOCK_ID = COption::GetOptionString($module_id, 'iblock_nodasoft');
		    $PROP_ID = COption::GetOptionString($module_id, 'prop_nodasoft');
		    $BREND_ID = COption::GetOptionString($module_id, 'brend_nodasoft');
		    $PRODUCT = COption::GetOptionString($module_id, 'product_nodasoft');
		    $LINK = COption::GetOptionString($module_id, 'link_nodasoft');

		    $this->arResult["PRODUCT"] = array();
		    $this->arResult["PAGES"] = array(
			    20,
			    40,
			    60
		    );
		    if (intval($_REQUEST["limit"]) && in_array(intval($_REQUEST["limit"]), $this->arResult["PAGES"]))
			    {
				    $this->arResult["PAGE"] = intval($_REQUEST["limit"]);
			    }
		    else
			    {
				    $this->arResult["PAGE"] = 20;
			    }

		    $this->arResult["page_num"] = 1;
		    if (intval($_REQUEST["page"]))
			    {
				    $this->arResult["page_num"] = intval($_REQUEST["page"]);
			    }

		    $this->arResult["DB_ITEMS"] = array();

		    if ($PRODUCT == "REMOTE" )
			    {
				    $result=$this->getArticles($LINK);
			    }
		    else
			    {
				    $result=$this->getProducts($IBLOCK_ID, $BREND_ID, $PROP_ID);
			    }

		    if($result)
			    {
				    $this->arResult["viewMode"] = "";
				    if (!empty($_REQUEST["viewMode"]))
					    {
						    $this->arResult["viewMode"] = htmlspecialcharsbx($_REQUEST["viewMode"]);
					    }

				    $db_items = new CDBResult;
				    $db_items->InitFromArray($this->arResult["DB_ITEMS"]);
				    $db_items->NavStart($this->arResult["PAGE"], false, $this->arResult["page_num"]);
				    while ($item = $db_items->Fetch())
					    {
						    $this->arResult["ITEMS"][] = $item;
					    }
				    $this->arResult["COUNT"] = $db_items->SelectedRowsCount();
				    $this->arResult["NAV_STRING"] = $db_items->GetPageNavStringEx($navComponentObject, "", "tecdoc", "N");

				    $this->IncludeComponentTemplate("group");
			    }
		    else
			    {
				    $this->IncludeComponentTemplate("404");
			    }
	    }

    public function getProducts($IBLOCK_ID, $BREND_ID, $PROP_ID)
	    {
	    	$module_id = "nodasoft.tecdoc";
		    $PRODUCT = COption::GetOptionString($module_id, 'product_nodasoft');
			$LINK = COption::GetOptionString($module_id, 'link_nodasoft');

		    if (!$IBLOCK_ID || !$PROP_ID)
			    {
				    $this->arResult["ERROR"] = "SETTINGS";
			    }

		    if ($this->arResult["ERROR"] == "")
			    {
				    $articles = $this->Tecdoc->getArticles(intval($_REQUEST["modelVariant"]), intval($_REQUEST["group"]));
				    if($articles=="") return false;
				    $arArtBrends = $arArtNumbers = array();
				    $articlesWithkeys = array();
				    foreach ($articles as $article)
					    {
						    $arArtNumbers[] = $article["number"];
						    $arArtBrends[$article["number"]] = $article["brandName"];
						    $articlesWithkeys[$article["number"]] = $article;
					    }

				    if (empty($arArtNumbers))
					    {
						    $arArtNumbers = 0;
					    }
				    $arFilter = array(
					    "IBLOCK_ID" => $IBLOCK_ID,
					    "PROPERTY_" . $PROP_ID => $arArtNumbers,
					    "ACTIVE" => "Y",
				    );
				    $arSelect = array(
					    "ID",
					    "NAME",
					    "PROPERTY_" . $PROP_ID,
					    "PROPERTY_" . $BREND_ID,
					    "DETAIL_PAGE_URL",
					    "PREVIEW_PICTURE",
					    "DETAIL_PICTURE",
					    "NAME",
				    );
				    $result = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				    while ($arItem = $result->GetNext())
					    {
						    $pict = 0;
						    if ($arItem["PREVIEW_PICTURE"] > 0)
							    {
								    $pict = $arItem["PREVIEW_PICTURE"];
							    }
						    elseif ($arItem["DETAIL_PICTURE"] > 0)
							    {
								    $pict = $arItem["DETAIL_PICTURE"];
							    }
						    if ($pict > 0)
							    {
								    $file = CFile::ResizeImageGet($pict, array(
									    "width" => 300,
									    "height" => 300
								    ), BX_RESIZE_IMAGE_PROPORTIONAL);
								    $arItem["SRC"] = $file['src'];
							    }

						    if ($BREND_ID > 0)
							    {
								    if (isset($arItem["PROPERTY_" . $BREND_ID . "_VALUE"] ) && ($arItem["PROPERTY_" . $BREND_ID . "_VALUE"] === $arArtBrends[$arItem["PROPERTY_" . $PROP_ID . "_VALUE"]]))
									    {
										    $this->arResult["DB_ITEMS"][] = array(
											    "ID" => $arItem["ID"],
											    "BRAND" => $arItem["PROPERTY_" . $BREND_ID . "_VALUE"],
											    "ART" => $arItem["PROPERTY_" . $PROP_ID . "_VALUE"],
											    "NAME" => $arItem["NAME"],
											    "SRC" => $arItem["SRC"],
											    "LINK" => $arItem["DETAIL_PAGE_URL"],
										    );
									    	unset($articlesWithkeys[$arItem["PROPERTY_" . $PROP_ID . "_VALUE"]]);

									    }

							    }
						    else
							    {
								    $this->arResult["DB_ITEMS"][] = array(
									    "ID" => $arItem["ID"],
									    "BRAND" => $arArtBrends[$arItem["PROPERTY_" . $PROP_ID . "_VALUE"]],
									    "ART" => $arItem["PROPERTY_" . $PROP_ID . "_VALUE"],
									    "NAME" => $arItem["NAME"],
									    "SRC" => $arItem["SRC"],
									    "LINK" => $arItem["DETAIL_PAGE_URL"],
								    );
								    unset($articlesWithkeys[$arItem["PROPERTY_" . $PROP_ID . "_VALUE"]]);

							    }
					    }

					if ($PRODUCT == "ALL") {
						foreach ($articlesWithkeys as $key => $art) {
							$this->arResult["DB_ITEMS"][] = array(
							    "ID" => $art["id"],
							    "BRAND" => $art["brandName"],
							    "ART" => $art["number"],
							    "NAME" => $art["description"],
							    "SRC" => $art["imageUrl"],
							    "LINK" => str_replace(array(
								    "{BRAND}",
								    "{NUMBER}"
							    ), array(
								    $art["brandName"],
								    $art["number"]
							    ), $LINK),
						    );
						}
					}
			    }

		    return true;


	    }

    public function getArticles($LINK)
	    {
		    $articles = $this->Tecdoc->getArticles(intval($_REQUEST["modelVariant"]), intval($_REQUEST["group"]));
		    if($articles=="") return false;
		    foreach ($articles as $art)
			    {
				    $this->arResult["DB_ITEMS"][] = array(
					    "ID" => $art["id"],
					    "BRAND" => $art["brandName"],
					    "ART" => $art["number"],
					    "NAME" => $art["description"],
					    "SRC" => $art["imageUrl"],
					    "LINK" => str_replace(array(
						    "{BRAND}",
						    "{NUMBER}"
					    ), array(
						    $art["brandName"],
						    $art["number"]
					    ), $LINK),
				    );
			    }
		    return true;
	    }

	}