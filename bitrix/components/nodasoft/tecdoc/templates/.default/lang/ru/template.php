<?
$MESS["NTD_SERCH_PLACEHOLDER"]="Начните вводить слово...";
$MESS["NTD_ALL"]="Все";
$MESS["NTD_CAR"]="Легковые";
$MESS["NTD_CAR_BIG"]="Грузовые";
$MESS["NTD_MODEL"]="Модель";
$MESS["NTD_YEAR_CREATE"]="Год выпуска";
$MESS["NTD_MODIFICATION"]="Модификация";
$MESS["NTD_CODE_DVIG"]="Код двигателя";
$MESS["NTD_DVIG"]="Двигатель";
$MESS["NTD_POWER"]="Мощность";
$MESS["NTD_OIL"]="Топливо";
$MESS["NTD_LITR"]=" л.";
$MESS["NTD_POWER_CO"]=" л.с.";
$MESS["NTD_AVTO_DETAIL"]="Автозапчасти";
$MESS["NTD_OPEN_ALL"]="Открыть все";
$MESS["NTD_CLOSE_ALL"]="Закрыть все";
$MESS["NTD_POPULAR_CAT"]="Популярные категории";
$MESS["NTD_TEXT_404"]='	<p>Сервис получения данных TecDoc в данный момент недоступен. Скорее всего это произошло потому, что:</p>
														<p>- вы достигли ограничений в демо-версии</p>
														<p>- вы достигли ограничения вызовов данной услуги в минуту/час/сутки на вашем тарифе. Список тарифов: <a href="http://tecdoc.abcp.ru/pricing">http://tecdoc.abcp.ru/pricing</a></p>
														<p>- вы указали реквизиты, для которых данная услуга недоступна</p>
														<p>- вы не оплатили услугу за текущий месяц</p>
														<p>Рекомендуем обновить страницу через несколько минут.</p>
														<p>Если результат отрицательный проверьте правильность заполнения реквизитов в настройках модуля и состояние услуги в Личном кабинете по адресу: <a href="http://cp.abcp.ru">http://cp.abcp.ru</a></p>
													';
$MESS["NTD_CROUP_1"]="Интервал регулировки";
$MESS["NTD_CROUP_2"]="Амортизатор";
$MESS["NTD_CROUP_3"]="Батарея";
$MESS["NTD_CROUP_4"]="Вентилятор";
$MESS["NTD_CROUP_5"]="Водяной насос / прокладка";
$MESS["NTD_CROUP_6"]="Водяной радиатор";
$MESS["NTD_CROUP_7"]="Воздушный фильтр";
$MESS["NTD_CROUP_8"]="Генератор";
$MESS["NTD_CROUP_81"]="Детали кузова/крыло/буфер";
$MESS["NTD_CROUP_9"]="Зеркала";
$MESS["NTD_CROUP_10"]="Испаритель";
$MESS["NTD_CROUP_11"]="Масла";
$MESS["NTD_CROUP_12"]="Катушка зажигания";
$MESS["NTD_CROUP_13"]="Лямбда-зонд";
$MESS["NTD_CROUP_14"]="Масляный фильтр";
$MESS["NTD_CROUP_15"]="Поликлиновый ремень";
$MESS["NTD_CROUP_16"]="Радиатор печки";
$MESS["NTD_CROUP_17"]="Рулевая тяга";
$MESS["NTD_CROUP_18"]="Свеча зажигания";
$MESS["NTD_CROUP_19"]="Стартер";
$MESS["NTD_CROUP_20"]="Ступица колеса";
$MESS["NTD_CROUP_21"]="Термостат";
$MESS["NTD_CROUP_22"]="Топливный насос";
$MESS["NTD_CROUP_23"]="Топливный фильтр";
$MESS["NTD_CROUP_24"]="Тормозная жидкость";
$MESS["NTD_CROUP_25"]="Тормозной диск";
$MESS["NTD_CROUP_26"]="Тормозные колодки";
$MESS["NTD_CROUP_27"]="тормозные шланги";
$MESS["NTD_CROUP_28"]="Фильтр салона";
$MESS["NTD_CROUP_29"]="Электроника двигателя";
$MESS["NTD_CHOW_TYPE_1"]="Плиткой";
$MESS["NTD_CHOW_TYPE_2"]="Списком";
$MESS["NTD_COUNT_PAGE"]="На странице:";
$MESS["NTD_SHOW_PRICE"]="Посмотреть цены";
$MESS["NTD_SELECT_PROP"]="Выберите Свойство привязки";
$MESS["NTD_FIRM_DETAIL"]="Фирма";
$MESS["NTD_CODE_DETAIL"]="Код детали";
$MESS["NTD_MODEL_DETAIL"]="Модель";
$MESS["NTD_GROUP_DETAIL"]="Группы деталей";

?>