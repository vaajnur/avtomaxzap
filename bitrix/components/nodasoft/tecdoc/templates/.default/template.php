<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs("https://yastatic.net/jquery/1.7.2/jquery.min.js");
$this->addExternalJs("https://yastatic.net/jquery-ui/1.8.16/jquery-ui.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/ui/custom-theme/jquery-ui-1.9.2.custom.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/ui/jquery-ui-i18n.min.js");
$this->addExternalJs("https://yastatic.net/jquery/fancybox/2.1.4/jquery.fancybox.min.js");
$this->addExternalCss("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.fancybox.css");
$this->addExternalJs("//static-files.nodacdn.net/js_libs/jquery/fb2/jquery.mousewheel-3.0.2.pack.js");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.min.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.clockpick.1.2.8.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl_minimized.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/jquery/jquery.jgrowl.css");
$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/jquery/jquery.easing.1.3.js");

$this->addExternalJs("//astatic.nodacdn.net/common.jscripts/tecdoc.js");
$this->addExternalCss("//astatic.nodacdn.net/common.jscripts/common.css");
?>

<div class="tecdocMain catalogTecdoc">
	<div class="tecdocTop">
		<script>

			$(document).ready(function () {
				$.data(document.body, 'searchTecdocMode', 'manufacturers');
			});
		</script>
		<input placeholder="<?=GetMessage("NTD_SERCH_PLACEHOLDER")?>" value="" id="searchTecdoc" name="searchTecdoc">
	</div>
	<div class="clearfix catalogTabs tecdocTabs">
		<ul class="tabs_table clearfix">

			<li class="<? if($arResult["CAR_TYPE"]==0) echo "active" ?>">
				<a href="<?=$arResult["URL"]?>"><?=GetMessage("NTD_ALL")?></a>
			</li>
			<li class="<? if($arResult["CAR_TYPE"]==1) echo "active" ?>">
				<a href="<?=$arResult["CAR_TYPE_1"]?>"><?=GetMessage("NTD_CAR")?></a>
			</li>
			<li class="<? if($arResult["CAR_TYPE"]==2) echo "active" ?>">
				<a href="<?=$arResult["CAR_TYPE_2"]?>"><?=GetMessage("NTD_CAR_BIG")?></a>
			</li>

			<? foreach (range('A', 'Z') as $letter):  ?>
				<? if($letter!="X"): ?>
					<li class="<?if($arResult["LITERA"]!="" && $letter==$arResult["LITERA"]) echo "active"?>">
						<a href="<?=$APPLICATION->GetCurPageParam("litera=".$letter,array("litera"))?>"><?=$letter?></a>
					</li>
				<? endif ?>
			<? endforeach ?>

		</ul>
	</div>
	<div class="tecdocCont">
		<? foreach($arResult["MANUFACTURES"] as $liter=>$manufactures): ?>

			<div class="listLine">
				<div class="wordBold">
					<?=$liter?>
				</div>
				<div class="brandsList">
					<ul>
						<? foreach($manufactures as $manufactura): ?>
							<li class="liSearch ">
								<a class="forSearch" href="<?=$manufactura["url"]?>"><?=$manufactura["name"]?></a>
							</li>
						<? endforeach ?>
					</ul>
				</div>
			</div>
		<? endforeach ?>

	</div>

</div>