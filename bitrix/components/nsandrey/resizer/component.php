<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!extension_loaded('imagick'))
{
	ShowError('ImageMagick '.GetMessage("NSANDREY_RESIZE_NE_USTANOVLEN"));
	return;
}

CModule::IncludeModule('nsandrey.resize');

$arParams['RESIZE_WIDTH'] = (int)$arParams['RESIZE_WIDTH'] > 0 ? $arParams['RESIZE_WIDTH'] : 0;
$arParams['RESIZE_HEIGHT'] = (int)$arParams['RESIZE_HEIGHT'] > 0 ? $arParams['RESIZE_HEIGHT'] : 0;

$arResult['IMAGE_ALT'] = trim($arParams['IMAGE_ALT']);
$arResult['IMAGE_CSS_CLASSES'] = trim(implode(' ', $arParams['IMAGE_CSS_CLASSES']));

// thumb
$image = new CNASThumb(
	$arParams['IMAGE'],
	'/' . $arParams['RESIZE_WIDTH'] . 'x' . $arParams['RESIZE_HEIGHT'] . '/',
	md5(serialize($arParams)),
	$arParams['NO_IMAGE']
);

if (!$image->isValid())
{
	ShowError(GetMessage("NSANDREY_RESIZE_KOMPONENT_NE_NASTROE"));
	return;
}

$proceededImagePath = $image->getFromCache();

// source image
if ($arParams['WATERMARK_ON_SOURCE'] == 'Y')
{
	// source image watermark
	$sourceImage = new CNASThumb(
		$arParams['IMAGE'],
		'/0x0/',
		md5(serialize($arParams)) . '_source',
		$arParams['NO_IMAGE']
	);

	$fullImagePath = $sourceImage->getFromCache();
}
else
{
	$fullImagePath = $image->getSourceImagePath();
}


if ($proceededImagePath === false || $fullImagePath === false)
{
	// resize
	if (!empty($arParams['RESIZE_TYPE']) && $arParams['RESIZE_TYPE'] != 'NONE')
	{
		$image->resize(
			$arParams['RESIZE_WIDTH'],
			$arParams['RESIZE_HEIGHT'],
			$arParams['RESIZE_TYPE'],
			$arParams['RESIZE_BACKGROUND']
		);
	}

	// watermark
	if ($arParams['WATERMARK_TYPE'] == 'IMAGE' || $arParams['WATERMARK_TYPE'] == 'TEXT')
	{
		$watermark = new CNASWatermark();

		switch ($arParams['WATERMARK_TYPE'])
		{
			case 'TEXT':
				$watermark->setText(
					$arParams['WATERMARK_TEXT'],
					$arParams['WATERMARK_FONT_SIZE'],
					$arParams['WATERMARK_FONT_COLOR'],
					$arParams['WATERMARK_FONT']
				);
				break;

			case 'IMAGE':
				$watermark->setImage($arParams['WATERMARK_IMAGE']);
				break;

			default:
				break;
		}

		$watermark->setAngle((int)$arParams['WATERMARK_ANGLE']);
		$watermark->setOpacity((int)$arParams['WATERMARK_OPACITY']);

		$image->addWatermark(
			$watermark,
			$arParams['WATERMARK_FILL'],
			array(
				isset($arParams['WATERMARK_POSITION_X']) ? $arParams['WATERMARK_POSITION_X'] : null,
				isset($arParams['WATERMARK_POSITION_Y']) ? $arParams['WATERMARK_POSITION_Y'] : null
			)
		);

		if ($arParams['WATERMARK_ON_SOURCE'] == 'Y')
		{
			$sourceImage->addWatermark(
				$watermark,
				$arParams['WATERMARK_FILL'],
				array(
					isset($arParams['WATERMARK_POSITION_X']) ? $arParams['WATERMARK_POSITION_X'] : null,
					isset($arParams['WATERMARK_POSITION_Y']) ? $arParams['WATERMARK_POSITION_Y'] : null
				)
			);

			$fullImagePath = $sourceImage->getProceededImagePath();
		}
	}

	$proceededImagePath = $image->getProceededImagePath();
}

$arResult['IMAGE_SOURCE'] = $fullImagePath;
$arResult['IMAGE_RESIZED'] = $proceededImagePath;

$this->IncludeComponentTemplate();