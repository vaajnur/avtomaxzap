<?
use Bitrix\Main\Localization\Loc;

$module_id = "ram.watermark";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT === 'D')
{
	$APPLICATION->AuthForm(Loc::getMessage("ram.watermark_ACCESS_DENIED"));
}

Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('ram.watermark');

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");

IncludeModuleLangFile(__FILE__);

$uploadDir = \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload");

$aTabs = array(
	Array("DIV" => "params", "TAB" => Loc::getMessage("ram.watermark_TAB_PARAMS"), "TITLE" => Loc::getMessage("ram.watermark_TAB_PARAMS_TITLE")),
	Array("DIV" => "rights", "TAB" => Loc::getMessage("ram.watermark_TAB_RIGHTS"), "TITLE" => Loc::getMessage("ram.watermark_TAB_RIGHTS_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update.$RestoreDefaults)>0 && check_bitrix_sessid() && $MODULE_RIGHT === 'W')
{
	if (strlen($RestoreDefaults) > 0)
	{
		$z = \CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while ($zr = $z->Fetch())  $APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
	}
	else
	{
		\Bitrix\Main\Config\Option::set('ram.watermark', 'include_files', htmlspecialcharsbx($_REQUEST['ramwatermark']['include_files']));
		\Bitrix\Main\Config\Option::set('ram.watermark', 'exclude_files', htmlspecialcharsbx($_REQUEST['ramwatermark']['exclude_files']));
	}
	
	LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&".$tabControl->ActiveTabParam());
}

$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&amp;lang=<?echo LANG?>" name="ram.watermark_settings">
<?=bitrix_sessid_post();?>
<?
$tabControl->BeginNextTab();
$includeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "include_files", "");
$excludeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "exclude_files", "/bitrix/admin/1c_exchange.php\n");
?>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_INCLUDE_FILES")?></td>
	<td width="50%">
		<textarea style='width: 300px; height: 60px;' name="ramwatermark[include_files]"><?=$includeFiles?></textarea>
	</td>
</tr>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_EXCLUDE_FILES")?></td>
	<td width="50%">
		<textarea style='width: 300px; height: 60px;' name="ramwatermark[exclude_files]"><?=$excludeFiles?></textarea>
	</td>
</tr>
<?

$tabControl->BeginNextTab();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

if ($MODULE_RIGHT === 'W')
{
	$tabControl->Buttons();?>

	<script language="JavaScript">
	function confirmRestoreDefaults()
	{
		return confirm('<?=AddSlashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>');
	}
	</script>
	<input type="submit" name="Update" value="<?=Loc::getMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?=Loc::getMessage("MAIN_RESET")?>">
	<input type="submit" name="RestoreDefaults" title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirmRestoreDefaults();" value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS")?>">

	<?
}
?>

<?$tabControl->End();?>
</form>